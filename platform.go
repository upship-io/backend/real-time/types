package realtime

type Platform string

const (
	PlatformUnknown Platform = "UNKNOWN"
	PlatformIOS     Platform = "IOS"
	PlatformAndroid Platform = "ANDROID"
	PlatformWeb     Platform = "WEB"
)

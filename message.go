package realtime

import "encoding/json"

type (
	MessageType string
	MessageData struct {
		ClientID string      `json:"clientId"`
		Type     MessageType `json:"type"`
		Payload  interface{} `json:"payload"`
	}
	Message struct {
		Pattern string      `json:"pattern"`
		Data    MessageData `json:"data"`
	}
)

const (
	OneToOne MessageType = "ONE_TO_ONE"
	Group    MessageType = "GROUP"
)

func UnmarshallMessage(data []byte) (Message, error) {
	var r Message
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Message) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

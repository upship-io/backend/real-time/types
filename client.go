package realtime

import (
	"golang.org/x/net/websocket"
)

type ClientID string
type Client struct {
	ID                        ClientID           `json:"id"`
	Platform                  Platform           `json:"platform"`
	AuthorizationToken        string             `json:"authorizationToken"`
	DecodedAuthorizationToken AuthorizationToken `json:"-"`
	connection                *websocket.Conn
}

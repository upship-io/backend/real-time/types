package realtime

type AuthorizationToken struct {
	Id          string `json:"id"`
	Key         string `json:"key"`
	Environment string `json:"environment"`
	Domain      string `json:"domain"`
	DomainId    string `json:"domainId"`
	Iat         int    `json:"iat"`
	Exp         int    `json:"exp"`
}
